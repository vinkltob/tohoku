#!/usr/bin/env python3

import torch
import torchvision

from PIL import Image

import boltseg.info as binfo
import boltseg.utils as butils
from boltseg.dataset import BoltDataset, split_dataset

def test_check_dataset():
    rgb_names = butils.rgb_names()
    mask_names = butils.mask_names()
    for name in rgb_names:
        assert name in mask_names, "Mask for " + name + " does not exist. Annotation required."
        rgb_path = butils.rgb_name2path(name)
        img = Image.open(rgb_path)
        assert img.size[:2] == (butils.IMAGE_WIDTH, butils.IMAGE_HEIGHT), "Image " + name + " has incorrect dimensions."

def test_split_dataset():
    transform = torchvision.transforms.ToTensor()
    train, val, test = split_dataset(0.2, 0.2, transform)
    assert len(train) + len(val) + len(test) == butils.dataset_size()

def test_masks():
    transform = torchvision.transforms.ToTensor()
    train, val, test = split_dataset(0.2, 0.2, transform)
    for ds in [train, val, test]:
        for img, mask in ds:
            for class_idx in mask.unique():
                assert class_idx <= binfo.NUM_CLASSES
                assert class_idx >= 0