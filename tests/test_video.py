import cv2
import numpy as np
import boltseg.utils as butils
import torch

cap = cv2.VideoCapture('tests/testing_video.mp4')

if not cap.isOpened():
    print("Error: Could not open video.")
    exit()

out = cv2.VideoWriter('tests/output_video.mp4',             # filename
                      cv2.VideoWriter_fourcc(*'mp4v'),     # codec
                      cap.get(cv2.CAP_PROP_FPS),            # fps
                      (int(cap.get(3)), int(cap.get(4))) )    # frame size

#mask = butils.load_mask("20231210_103307").numpy()

# Create a mask (you can replace this with your own mask creation logic)
#mask = cv2.imread('your_mask.png', cv2.IMREAD_GRAYSCALE)

while True:
    ret, frame = cap.read()

    # Video ended
    if not ret:
        break

    #frame = torch.tensor(np.transpose(np.array(frame), (2, 0, 1)),dtype=torch.float32)
    #masked_frame = butils.applymask(frame,mask).numpy()
    masked_frame = frame
    # Write the processed frame to the output video
    out.write(masked_frame)

    # Can comment this if no visualization
    #cv2.imshow('Processed Frame', masked_frame)
    #if cv2.waitKey(25) & 0xFF == ord('q'):
    #    break

cap.release()
out.release()
cv2.destroyAllWindows()