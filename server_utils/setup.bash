#!/usr/bin/env bash

export CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
export CUDA_DEVICE_ORDER=PCI_BUS_ID

ml PyTorch/2.0.1-foss-2022a-CUDA-11.7.0
ml JupyterLab/3.5.0-GCCcore-11.3.0
ml SciPy-bundle/2022.05-foss-2022a
ml OpenCV/4.7.0-foss-2022a-CUDA-11.7.0-contrib
ml wandb/0.13.6-GCC-11.3.0