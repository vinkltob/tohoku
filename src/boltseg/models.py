import torch
import torch.nn as nn

from abc import ABC, abstractmethod

from .info import IMAGE_HEIGHT, IMAGE_WIDTH

class Model(nn.Module, ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def forward(self, img):
        raise NotImplementedError
    
    def train_step(self, train_loader, criterion, optimizer, device):
        self.train()
        total_loss, n_iters = 0.0, 0
        for img, mask in train_loader:
            img, mask = img.to(device), mask.to(device)

            # forwad pass - generate logits
            logits = self(img)

            # compute loss
            loss = criterion(logits, mask)

            # update training step
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # bookkeeping
            total_loss += loss.item()
            n_iters += 1
            del loss

        info = {
            "train_loss" : total_loss / n_iters
        }

        return info

    @torch.no_grad()
    def validation_step(self, val_loader, criterion, device):
        self.eval()
        total_loss, n_iters = 0.0, 0
        for img, mask in val_loader:
            img, mask = img.to(device), mask.to(device)

            # forward pass - generate logits
            logits = self(img)

            # compute loss
            loss = criterion(logits, mask)

            # bookkeeping
            total_loss += loss.item()
            n_iters += 1

        info = {
            "val_loss" : total_loss / n_iters
        }

        return info
        
    def test_step(self, test_loader, criterion, device):
        info = self.validate(test_loader, criterion, device)
        info["test_loss"] = info["val_loss"]
        del info["val_loss"]
        return info
    
class MobilenetSmallModel(Model):
    def __init__(self, num_classes=3, pretrained=True):
        super().__init__()

        from fastseg import MobileV3Small

        if pretrained:
            self.net = MobileV3Small.from_pretrained()
            last_layer = nn.Conv2d(128, num_classes, kernel_size=1, stride=1)
            self.net.last = last_layer
        else:
            self.net = MobileV3Small(num_classes=num_classes)

    def forward(self, img):
        return self.net(img)
    
class UnetModel(Model):
    def __init__(self, num_classes=3):
        super().__init__()
        self.num_classes = num_classes

        self.initialize_net()
        
    def initialize_net(self):

        self.c11 = nn.Conv2d(3, 64, kernel_size=3, padding=1)  
        self.c12 = nn.Conv2d(64, 64, kernel_size=3, padding=1)
        self.down1 = nn.MaxPool2d(kernel_size=2, stride=2)  

        self.c21 = nn.Conv2d(64, 128, kernel_size=3, padding=1)
        self.c22 = nn.Conv2d(128, 128, kernel_size=3, padding=1)
        self.down2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.c31 = nn.Conv2d(128, 256, kernel_size=3, padding=1)
        self.c32 = nn.Conv2d(256, 256, kernel_size=3, padding=1)
        self.down3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.c41 = nn.Conv2d(256, 512, kernel_size=3, padding=1)
        self.c42 = nn.Conv2d(512, 512, kernel_size=3, padding=1)
        self.down4 = nn.MaxPool2d(kernel_size=2, stride=2)
    
        self.c51 = nn.Conv2d(512, 1024, kernel_size=3, padding=1)
        self.c52 = nn.Conv2d(1024, 1024, kernel_size=3, padding=1)

        self.up4 = nn.ConvTranspose2d(1024, 512, kernel_size=2, stride=2)
        self.d41 = nn.Conv2d(1024, 512, kernel_size=3, stride=1, padding=1)
        self.d42 = nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1)

        self.up3 = nn.ConvTranspose2d(512, 256, kernel_size=2, stride=2)
        self.d31 = nn.Conv2d(512, 256, kernel_size=3, stride=1, padding=1)
        self.d32 = nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1)

        self.up2 = nn.ConvTranspose2d(256, 128, kernel_size=2, stride=2)
        self.d21 = nn.Conv2d(256, 128, kernel_size=3, stride=1, padding=1)
        self.d22 = nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1)

        self.up1 = nn.ConvTranspose2d(128, 64, kernel_size=2, stride=2)
        self.d11 = nn.Conv2d(128, 64, kernel_size=3, stride=1, padding=1)
        self.d12 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)

        self.out11 = nn.Conv2d(64, self.num_classes, kernel_size=1, stride=1, padding=0)
        self.upsample = nn.Upsample(size=(IMAGE_HEIGHT, IMAGE_WIDTH), mode="nearest")

        self.activation = torch.nn.functional.relu
        
    def rescon_slice(self, residual, up):
        residual_shape = residual.shape
        up_shape = up.shape

        diff1 = residual_shape[-2] - up_shape[-2]
        diff1 = diff1 // 2

        diff2 = residual_shape[-1] - up_shape[-1]
        diff2 = diff2 // 2

        return residual[:, :, diff1:diff1+up_shape[-2], diff2:diff2+up_shape[-1]]
    
    def forward(self, img):
        
        activation = self.activation
        # encoder 1
        c11_o = activation(self.c11(img))
        c12_o = activation(self.c12(c11_o))
        down1_o = self.down1(c12_o)

        # encoder 2
        c21_o = activation(self.c21(down1_o))
        c22_o = activation(self.c22(c21_o))
        down2_o = self.down2(c22_o)

        # encoder 3
        c31_o = activation(self.c31(down2_o))
        c32_o = activation(self.c32(c31_o))
        down3_o = self.down3(c32_o)

        # encoder 4
        c41_o = activation(self.c41(down3_o))
        c42_o = activation(self.c42(c41_o))
        down4_o = self.down4(c42_o)


        # encoder 5
        c51_o = activation(self.c51(down4_o))
        c52_o = activation(self.c52(c51_o))

        # decoder 4
        up4_o = self.up4(c52_o)
        d41_o = activation(self.d41(torch.cat([up4_o, self.rescon_slice(c42_o, up4_o)], dim=1))) # dim=0 is the batch dimensions
        d42_o = activation(self.d42(d41_o))

        # encoder 3
        up3_o = self.up3(d42_o)
        d31_o = activation(self.d31(torch.cat([up3_o, self.rescon_slice(c32_o, up3_o)], dim=1)))
        d32_o = activation(self.d32(d31_o))

        # encoder 2
        up2_o = self.up2(d32_o)
        d21_o = activation(self.d21(torch.cat([up2_o, self.rescon_slice(c22_o, up2_o)], dim=1)))
        d22_o = activation(self.d22(d21_o))

        # encoder 1
        up1_o = self.up1(d22_o)
        d11_o = activation(self.d11(torch.cat([up1_o, self.rescon_slice(c12_o, up1_o)], dim=1)))
        d12_o = activation(self.d12(d11_o))

        # 1x1 convolution
        out = self.out11(d12_o)
        
        # upsample mask
        out = self.upsample(out)
        return out