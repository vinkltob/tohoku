#!/usr/bin/env python3

import cv2
import glob
from .info import *

import torch
import torchvision
import scipy.sparse as sp_sparse

from PIL import Image

import matplotlib.pyplot as plt

def rgb_generator():
    return (glob.glob(DATASET_RGB_PATH + "/*"))

def seg_generator():
    return (glob.glob(DATASET_SEG_PATH + "/*"))

def mask_generator():
    return (glob.glob(DATASET_MASK_PATH + "/*"))

def rgb_names():
    return [os.path.basename(path).split(".")[0] for path in rgb_generator()]

def seg_names():
    return [os.path.basename(path).split(".")[0] for path in seg_generator()]

def mask_names():
    return [os.path.basename(path).split(".")[0] for path in mask_generator()]

def rgb_name2path(name):
    return os.path.join(DATASET_RGB_PATH, name + ".jpg")

def seg_name2path(name):
    return os.path.join(DATASET_SEG_PATH, name + ".jpg")

def mask_name2path(name):
    return os.path.join(DATASET_MASK_PATH, name + ".npz")

def dataset_size():
    return len(rgb_names())

def load_image(name):
    return torchvision.transforms.functional.to_tensor(Image.open(rgb_name2path(name)))

def load_mask(name):
    return torch.from_numpy(sp_sparse.load_npz(mask_name2path(name)).toarray())

def torch2pil(tensor):
    assert tensor.shape[0] == 3  # rgb channels
    return torchvision.transforms.functional.to_pil_image(tensor)

def applymask(img, mask):
    assert img.shape[0] == 3 # rgb channels
    assert img.dtype == torch.float32

    img = img.clone()
    img_max = img.max()

    for class_idx in mask.unique():
        class_idx = class_idx.item()
        if INT2CLASS[class_idx] == "background":
            continue

        idx, _ = INT2COLOR[class_idx]

        mask2 = torch.zeros_like(img)
        mask2[idx, :, :] = mask

        img[(mask2 == class_idx)] = img_max

    return img

def show_img(img, title=None):
    assert img.shape[0] == 3 # rgb channels 
    img = img.clone()
    img = (img - img.min()) / (img.max() - img.min())
    plt.imshow(img.permute(1, 2, 0))
    plt.xticks([]),plt.yticks([])
    if title:
        plt.title(title)
    plt.show()

def compare_images(*imgs, figsize=None):
    for img in imgs:
        assert img.shape[0] == 3 # rgb channels
    n_images = len(imgs)

    if figsize is None:
        figsize = (4 * n_images, 4)

    plt.figure(figsize=figsize)

    for i, img in enumerate(imgs):
        img = img.clone()
        img = (img - img.min()) / (img.max() - img.min())
        plt.subplot(1, n_images, i+1)
        plt.imshow(img.permute(1, 2, 0))
        plt.xticks([]),plt.yticks([])
    plt.show()

def print_config(config):
    print("{")
    for key, value in config.items():
        print(f"  {key}  :  {value}")
    print("}")

def segment_video(input_path, output_path, model):
    device = next(model.parameters()).device
    cap = cv2.VideoCapture(input_path)
    assert cap.isOpened(), f"Error: Could not open video {input_path}"

    codec = cv2.VideoWriter_fourcc(*'mp4v')
    fps = cap.get(cv2.CAP_PROP_FPS)
    video_size = (IMAGE_WIDTH, IMAGE_HEIGHT)

    out = cv2.VideoWriter(output_path,codec, fps, video_size)

    while True:
        ret, frame = cap.read()
        if not ret:
            break
        
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        # resize frame
        img = Image.fromarray(frame).resize(video_size) 
        img = torchvision.transforms.functional.to_tensor(img).float().to(device)
        
        mask = model(img.unsqueeze(0)).squeeze().argmax(dim=0).cpu()
        
        img = img.cpu()
        img = applymask(img, mask)
        img = torch2pil(img)
        
        out.write(cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR))

    cap.release()
    out.release()
    cv2.destroyAllWindows()