#!/usr/bin/env python3

import os
import numpy as np

IMAGE_HEIGHT = 512
IMAGE_WIDTH = 682

DATASET_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../dataset")
DATASET_RGB_PATH = os.path.join(DATASET_PATH, "rgb")
DATASET_SEG_PATH = os.path.join(DATASET_PATH, "seg")
DATASET_MASK_PATH = os.path.join(DATASET_PATH, "mask")

# Class integer -> class name
INT2CLASS = {
    0 : "background",
    1 : "bolt",
    2 : "nut"
}

# Class name -> class integer
CLASS2INT = {c: i for i, c in INT2CLASS.items()}

# Number of unique classes
NUM_CLASSES = len(INT2CLASS)

# Class integer -> RGB index and color
INT2COLOR = {
    0 : (0, 255),  # red
    1 : (0, 255),  # red
    2 : (2, 255)   # blue
}

# Annotation info
ANNOTATION_INFO = {
    "model_path" : "/home/kuba/fun/urob/hw03/FastSAM/weights/FastSAM-x.pt",
    "imgz" : 1024,
    "device" : "cuda"
}

## Training config
TRAINING_CONFIG = {
    "project_name" : "boltseg",
    "experiment_name" : "my_experiment",
    "learning_rate" : 1e-3,
    "batch_size" : 8,
    "num_epochs" : 10,
}