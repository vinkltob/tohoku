#!/usr/bin/env python3

import torch
import torchvision
from torch.utils.data import Dataset
from PIL import Image

from abc import ABC, abstractmethod

import random
import scipy as sp

from . import utils as butils


class AugmentationModule(ABC):
    @abstractmethod
    def transform(self, img, mask):
        raise NotImplementedError


class Flip(AugmentationModule):
    def __init__(self, axis, p=0.5):
        self.p = p
        self.axis = axis

    def transform(self, img, mask):
        assert isinstance(img, torch.Tensor), "img must be a torch.Tensor"
        doit = random.random() < self.p
        img = self._transform_image(img, doit)
        mask = self._transform_mask(mask, doit)

        return img, mask

    def _transform_image(self, img, doit):
        assert isinstance(img, torch.Tensor), "img must be a torch.Tensor"
        if doit:
            img = torch.flip(img, [self.axis])
        return img

    def _transform_mask(self, mask, doit):
        assert isinstance(mask, torch.Tensor), "mask must be a torch.Tensor"
        if doit:
            mask = torch.flip(mask, [self.axis])
        return mask


class FlipHorizontal(Flip):
    def __init__(self, p=0.5):
        super().__init__(-1, p)


class FlipVertical(Flip):
    def __init__(self, p=0.5):
        super().__init__(-2, p)


class GaussianBlur(AugmentationModule):
    def __init__(self, sigma, kernel_sizes, p=0.5):
        self.sigma = sigma
        self.p = p
        self.kernel_sizes = kernel_sizes

    def transform(self, img, mask):
        assert isinstance(img, torch.Tensor), "img must be a torch.Tensor"
        doit = random.random() < self.p
        img = self._transform_image(img, doit)

        return img, mask

    def _transform_image(self, img, doit):
        assert isinstance(img, torch.Tensor), "img must be a torch.Tensor"
        if doit:
            k = random.choice(self.kernel_sizes)
            kernel_size = [k, k]
            img = torchvision.transforms.functional.gaussian_blur(
                img, kernel_size=kernel_size, sigma=self.sigma
            )
        return img


class BoltDataset(torch.utils.data.Dataset):
    def __init__(self, rgb_names, transform=None, augmentation_transforms=None):
        self.rgb_names = rgb_names
        self.transform = transform
        self.augmentation_transforms = augmentation_transforms

        if self.augmentation_transforms is not None:
            assert isinstance(
                self.augmentation_transforms, list
            ), "augmentation_transforms must be a list"
            for t in self.augmentation_transforms:
                assert isinstance(
                    t, AugmentationModule
                ), "augmentation_transforms must contain AugmentationModule objects"

    def __len__(self):
        return len(self.rgb_names)

    def __getitem__(self, idx):
        name = self.rgb_names[idx]

        # segmentation image
        img = butils.load_image(name)
        if self.transform is not None:
            img = self.transform(img)

        assert isinstance(img, torch.Tensor), "transform must return a torch.Tensor"

        # load mask
        mask = butils.load_mask(name).long()

        # perform augmentation
        if self.augmentation_transforms is not None:
            for t in self.augmentation_transforms:
                img, mask = t.transform(img, mask)

        # input, desired output
        return img, mask


def split_dataset(
    test_size,
    validation_size,
    transform=None,
    random_state=420,
    shuffle=True,
    augment=True,
):
    assert test_size > 0.0 and test_size < 1.0, "test_size must be in [0, 1]"
    assert (
        validation_size > 0.0 and validation_size < 1.0
    ), "validation_size must be in [0, 1]"
    assert (
        test_size + validation_size < 1.0
    ), "test_size + validation_size must be less than 1.0"

    random.seed(random_state)
    torch.manual_seed(random_state)

    rgb_names = butils.rgb_names()
    if shuffle:
        random.shuffle(rgb_names)

    augmentation_transforms = []
    if augment:
        augmentation_transforms.append(FlipHorizontal(p=0.5))
        augmentation_transforms.append(FlipVertical(p=0.5))
        augmentation_transforms.append(
            GaussianBlur(
                sigma=None, kernel_sizes=[3, 5, 7, 9, 11, 13, 15, 17, 19, 21], p=0.3
            )
        )

    dataset_size = butils.dataset_size()

    test_size = int(dataset_size * test_size)
    validation_size = int(dataset_size * validation_size)
    train_size = dataset_size - test_size - validation_size

    train_rgb_names = rgb_names[:train_size]
    train_dataset = BoltDataset(
        train_rgb_names,
        transform=transform,
        augmentation_transforms=augmentation_transforms,
    )

    val_rgb_names = rgb_names[train_size : train_size + validation_size]
    val_dataset = BoltDataset(
        val_rgb_names,
        transform=transform,
        augmentation_transforms=augmentation_transforms,
    )

    test_rgb_names = rgb_names[train_size + validation_size :]
    test_dataset = BoltDataset(
        test_rgb_names,
        transform=transform,
        augmentation_transforms=augmentation_transforms,
    )

    return train_dataset, val_dataset, test_dataset
