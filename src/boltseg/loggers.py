#!/usr/bin/env python3

import wandb

from abc import ABC, abstractmethod

class Logger(ABC):
    @abstractmethod
    def log(self, info, step):
        raise NotImplementedError
    
    def finish(self):
        pass

class WandbLogger(Logger):
    def __init__(self, config):
        wandb.login()

        assert "project_name" in config, "project_name not specified in config"
        assert "experiment_name" in config, "experiment_name not specified in config"
        project_name = config["project_name"]
        experiment_name = config["experiment_name"]
        wandb.init(project=project_name, name=experiment_name)

        wandb.config = config

    def log(self, info, step):
        wandb.log(info, step=step)

    def finish(self):
        wandb.finish()