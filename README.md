# bolt-seg
This python package does end-to-end training of a deep learning application segmenting bolts and nuts in images.


## Install 
```bash
pip3 install -e .
```

## Uninstall 
```bash
pip3 uninstall boltseg-project
```