from setuptools import setup, find_packages


print(find_packages(where='src', include=['boltseg', 'boltseg.*']))

setup(
    name='boltseg project',
    version='0.1.0',
    packages=find_packages(where='src', include=['boltseg', 'boltseg.*']),
    package_dir={'': 'src',
                 "boltseg" : "src/boltseg",}
)