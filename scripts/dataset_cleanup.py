#!/usr/bin/env python3

import glob
import os
import boltseg.utils as butils

rgb_names = butils.rgb_names()
seg_names = butils.seg_names()

for rgb_name in rgb_names:
    if rgb_name not in seg_names:
        print("Removing " + rgb_name + ".jpg and " + rgb_name + ".npz")
        rgb_path = butils.rgb_name2path(rgb_name)
        os.remove(rgb_path)
        mask_path = butils.mask_name2path(rgb_name)
        os.remove(mask_path)