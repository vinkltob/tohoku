#!/usr/bin/env python3

import boltseg.info as binfo
import boltseg.utils as butils

from boltseg.thirdparty.fastsam import FastSAM, FastSAMPrompt

import os
import scipy.sparse as sp_sparse
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

def apply_mask(img, mask, current_id):
    assert img.shape[:2] == mask.shape[:2]
    rgb_idx, color = binfo.INT2COLOR[current_id]
    img[mask, rgb_idx] = color
    return img

def annotate(rgb_path, rgb_name, model):
    img = np.array(Image.open(rgb_path))

    assert img.shape[:2] == (binfo.IMAGE_HEIGHT, binfo.IMAGE_WIDTH)


    current_class = "bolt"
    current_id = binfo.CLASS2INT[current_class]

    f, ax = plt.gcf(), plt.gca()
    ax.set_title("Annotating " + current_class + "s")
    pltim = plt.imshow(img)

    mng = plt.get_current_fig_manager()
    mng.resize(*mng.window.maxsize())

    mask = np.zeros((binfo.IMAGE_HEIGHT, binfo.IMAGE_WIDTH), dtype=np.uint8)

    everything_results = model(rgb_path, device=binfo.ANNOTATION_INFO["device"], retina_masks=True, imgsz=binfo.ANNOTATION_INFO["imgz"], conf=0.4, iou=0.9,)
    prompt_process = FastSAMPrompt(rgb_path, everything_results, device=binfo.ANNOTATION_INFO["device"])

    def onclick(event):
        nonlocal current_id, current_class, mask, img
        x, y = event.xdata, event.ydata

        # if click is outside of image, change annotated class
        if x is None or y is None:
            if current_class == "bolt":
                current_class = "nut"
            elif current_class == "nut":
                current_class = "bolt"
            current_id = binfo.CLASS2INT[current_class]
            ax.set_title("Annotating " + current_class + "s")
            plt.draw()

        x, y = int(x), int(y)
        annotated_masks = prompt_process.point_prompt(points=[[x, y]], pointlabel=[1])
        mask[annotated_masks[0]] = current_id

        # update image
        img = apply_mask(img, annotated_masks[0], current_id)
        pltim.set_data(img)
        plt.draw()

    f.canvas.mpl_connect('button_press_event', onclick)
    plt.show()

    # save segmented image
    segmented_path = os.path.join(binfo.DATASET_SEG_PATH, rgb_name + ".jpg") # image with mask applied
    Image.fromarray(img).save(segmented_path)

    # save mask
    mask_path = os.path.join(binfo.DATASET_MASK_PATH, rgb_name + ".npz") # scipy sparse matrix
    mask_sparse = sp_sparse.csr_matrix(mask)
    sp_sparse.save_npz(mask_path, mask_sparse)

    
seg_names = butils.seg_names()
mask_names = butils.mask_names()
rgb_names = butils.rgb_names()    

model = FastSAM(binfo.ANNOTATION_INFO["model_path"])

for rgb_name, rgb_path in zip(rgb_names, butils.rgb_generator()):

    # check if image is already annotated
    if rgb_name in seg_names and rgb_name in mask_names:
        continue  

    annotate(rgb_path, rgb_name, model)

