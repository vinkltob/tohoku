#!/usr/bin/env python3

import glob
from PIL import Image

from boltseg.info import DATASET_RGB_PATH, IMAGE_HEIGHT, IMAGE_WIDTH

for image_path in glob.glob(DATASET_RGB_PATH + "/*"):
    image = Image.open(image_path)
    w, h = image.size
    
    if w != IMAGE_WIDTH or h != IMAGE_HEIGHT:
        print("Resizing image: " + image_path)
        image = image.resize((IMAGE_WIDTH, IMAGE_HEIGHT)) ## PIL's convention is (width, height)    
        image.save(image_path)

print("Done resizing images.")